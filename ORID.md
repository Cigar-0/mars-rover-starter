## ORID

###  O：

This morning, we did code review first, we shared code to the team one by one, the main content is our yesterday's homework. 

It involves design patterns, observer patterns. I've seen this concept before but I've never actually implemented it. So it took more time to realize.

 Then we learned unit test, the importance of unit test and process of it.  In the afternoon, we learned TDD which means test driven development, I learned its concept. 

We played FizzBizz Game and then practiced to write the test and implement.  

### R：

During the process, my teammates saw my problem in my homework that I didn't realized, I also learned a lot from others solutions.

During the process, I have a strong memory for committing code in small steps.

### I：

Through code review, I discovered some loopholes in my thinking process. My consideration was not comprehensive enough, such as not considering null pointer exceptions.

TDD can detect and correct errors early, and detect problems in a timely manner; Keep a clean and concise Codebase in the development iteration cycle; Better organization and understanding of code structure; Enhanced code readability and maintainability, reducing testing costs; Reduced project risks; In some cases, it can improve development efficiency

### D：

In the future development process, I will also try to use TDD method. And submit the code in small steps.

