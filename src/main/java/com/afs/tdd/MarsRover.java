package com.afs.tdd;

import java.awt.*;
import java.util.List;

public class MarsRover {
    private final Location location;
    public MarsRover(Location location) {
        this.location = location;
    }


    public void executeCommand(Command command) {
        if (command.equals(Command.Move)) {
            move();
        }
        if(command.equals(Command.Left)) {
            turnLeft();
        }
        if(command.equals(Command.Right)) {
            turnRight();
        }

    }

    private void turnRight() {
        Direction nowDirection = location.getDirection();
        if(nowDirection.equals((Direction.North))) {
            location.setDirection(Direction.East);
        } else if (nowDirection.equals((Direction.South))){
            location.setDirection(Direction.West);
        } else if (nowDirection.equals((Direction.East))){
            location.setDirection(Direction.South);
        } else if (nowDirection.equals((Direction.West))){
            location.setDirection(Direction.North);
        }
    }

    private void move() {
        Direction nowDirection = location.getDirection();
        if(nowDirection.equals((Direction.North))) {
            location.setCoordinateY(location.getCoordinateY() + 1);
        } else if (nowDirection.equals((Direction.South))) {
            location.setCoordinateY(location.getCoordinateY() - 1);
        } else if (nowDirection.equals(Direction.East)) {
            location.setCoordinateX(location.getCoordinateX() + 1);
        } else if (nowDirection.equals(Direction.West)) {
            location.setCoordinateX(location.getCoordinateX() - 1);
        }
    }

    public Location getLocation() {
        return location;
    }
    public void turnLeft() {
        Direction nowDirection = location.getDirection();
        if(nowDirection.equals((Direction.North))) {
            location.setDirection(Direction.West);
        } else if (nowDirection.equals(Direction.South)) {
            location.setDirection(Direction.East);
        } else if (nowDirection.equals(Direction.East)) {
            location.setDirection(Direction.North);
        } else if (nowDirection.equals(Direction.West)) {
            location.setDirection(Direction.South);
        }
    }

    public void turnMultiCommand(List<Command> commandList) {
        for (Command command : commandList) {
            executeCommand(command);
        }
    }


}
